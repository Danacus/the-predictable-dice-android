package xyz.vanoverloop.thepredictabledice;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    TextView diceText;
    int[] choices = new int[13];
    public double[] weights = new double[13];
    int[] history = new int[13];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.diceText = findViewById(R.id.dice_text);

        for (int i = 0; i < 13; i++) {
            choices[i] = i;

            if (i > 1)
                weights[i] = 1f / 12f;

            history[i] = 0;
        }
    }

    public float diceProbability(int x) {
        int n = 6;

        if (x < 2 || x > 2 * n)
            return 0;
        else
            return (float)(n - Math.abs(x - (n + 1))) / (float)Math.pow(n, 2);
    }

    public void normalize(double[] array) {
        double sum = Arrays.stream(array).sum();
        for (int i = 0; i < array.length; i++) {
            array[i] /= sum;
        }
    }

    public void rollDice(View view) {
        this.diceText.setText(String.format("%d", generateRandom()));
    }

    public int generateRandom() {
        double[] w = new double[13];

        for (int i = 0; i < weights.length; i++) {
            w[i] = diceProbability(choices[i]) * weights[i];
        }

        normalize(w);

        int rnd = weightedRandom(w);

        int n = 4;
        weights[rnd] /= n;

        double[] add = new double[13];
        add[0] = 0;

        for (int i = 1; i < add.length; i++) {
            add[i] = diceProbability(choices[i]) * ((n - 1) * weights[rnd] / (n * 12 - 1));
        }

        normalize(add);

        for (int i = 0; i < choices.length; i++) {
            if (i != rnd)
                weights[i] += add[i];
        }

        history[rnd] += 1;

        return rnd;
    }

    public int weightedRandom(double[] weights) {
        double totalWeight = Arrays.stream(weights).sum();

        int randomIndex = -1;
        double random = Math.random() * totalWeight;
        for (int i = 0; i < this.choices.length; ++i) {
            random -= weights[i];
            if (random <= 0.0d) {
                randomIndex = i;
                break;
            }
        }

        return this.choices[randomIndex];
    }
}
